using UnityEngine;
using TMPro;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.AddressableAssets;

public class TestCubeManager : MonoBehaviour
{
    [SerializeField] private GameObject _cubePrefab;
    [SerializeField] private TextMeshProUGUI _amountOfCubesText;
    [SerializeField] private AssetReference _cubeReference;
    [SerializeField] private AssetReference _cubeAsbestosRef;
    private GameObject _spawnedAddressable;
    private GameObject _spawnedAsbestosAddress;
    private int _spaceCounter = 0;

    private void Awake()
    {
        Screen.SetResolution(1280, 720, FullScreenMode.Windowed, 60);
    }
    private void Start()
    {
        SpawnCubeFromPrefab();
        RefreshText();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            switch (_spaceCounter)
            {
                case 0:
                    {
                        SpawnCubeFromAddressable();
                    }
                    break;
                case 1:
                    {
                        DespawnAddressable();
                    }
                    break;
                case 2:
                    {

                    }
                    break;
            }

            _spaceCounter++;
            RefreshText();
        }
    }

    private void RefreshText()
    {
        _amountOfCubesText.text = $"child count: {transform.childCount.ToString()},counter: {_spaceCounter}";
    }

    private void SpawnCubeFromPrefab()
    {
        GameObject spawned = Instantiate(_cubePrefab, transform);
        spawned.transform.localPosition = Vector3.zero;
    }

    private void SpawnCubeFromAddressable()
    {
        _cubeReference.InstantiateAsync().Completed += (AsyncOperationHandle<GameObject> asyncOperationHandle) =>
        {
            _spawnedAddressable = asyncOperationHandle.Result;
            _spawnedAddressable.transform.SetParent(transform);
            _spawnedAddressable.transform.localPosition = new Vector3(2f, 0f, 0f);
            RefreshText();
        };

        _cubeAsbestosRef.InstantiateAsync().Completed += (AsyncOperationHandle<GameObject> asyncOperationHandle) =>
        {
            _spawnedAsbestosAddress = asyncOperationHandle.Result;
            _spawnedAsbestosAddress.transform.SetParent(transform);
            _spawnedAsbestosAddress.transform.localPosition = new Vector3(-2f, 0f, 0f);
            RefreshText();
        };
    }

    private void DespawnAddressable()
    {
        _cubeReference.ReleaseInstance(_spawnedAddressable);
        _cubeAsbestosRef.ReleaseInstance(_spawnedAsbestosAddress);
    }
}
